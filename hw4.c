/*
 * Thomas Keady
 * 600.120
 * 3/11/15
 * 516-729-9535
 * tkeady1
 * tkeady1@jhu.edu
 */

#include "list.h"
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


int main (int argc, char* argv[]) {
	
	if(argc < 2) {			
		printf("USAGE ./hw4 database.dat\n");
		return 1;		//make sure the user calls properly
	}
	
	FILE* databasePtr = fopen(argv[1], "r+b");
	if (databasePtr == NULL) {
		printf("Made file\n");
		databasePtr = fopen(argv[1], "w+b");
	}
	if (fgetc(databasePtr) == EOF) {
		// make file
		databasePtr = makeDatabase(databasePtr);
	}
		
	
	/*
	if (!databasePtr){			//make sure its a valid file
		return 1;
	}
	
	*/
	
	Node** semester = malloc(sizeof(Node*)*10);
	/*
	for (int i = 0; i < 10; ++i) {
		semester[i] == NULL;
	}*/
	
	
	int input;
	int dept, num, sem;
	//char* grade = malloc(sizeof(char)*3);;
	//char c = ' ';
	//char grade[2];
	//char* grade;
	char grade[2];
	//char grade, gradeSign;
	/*Course* empty = malloc(sizeof(Course));
	empty->div = "";
	empty->dept = 0;
	empty->num = 0;
	empty->c1 = 0;
	empty->c2 = 0;
	empty->title = "";
	*/
	Course* line = malloc(sizeof(Course));
	//printf("Size: %lu\n", sizeof(Course));
	
	do {
		printf("Menu options:\n0. Quit.\n1. Create new courses from a plain text file, adding them to the database.\n2. Add a single course to the database.\n3. Display all the details of a particular course.\n4. Display all the courses in the database.\n5. Change the name of a course\n6. Delete a course from a database\n7. Add a course to a semester listing.\n8. Remove a course from a semester listing.\n9. Display a semester course listing.\nChoice: \n");
		scanf("%d", &input);
		switch(input) 
		{
		case 0:	;
			break;
		case 1:
			printf("Please enter the name of the plain text file: \n");
			char fileName[255];
			scanf("%s", fileName);
			FILE* dataPtr = fopen(fileName, "r");
			if (dataPtr == NULL) {
				fprintf(stderr, "File does not exist\n");
				break;
			}
			//printf("got here\n");
			addToDatabase(databasePtr, fileName);
			
			break;
		case 2:	;

			/*printf("Please enter the course data (\"__.###.### #.# Title...\"):\n");
			char courseData[46];
			int matches = fscanf("%s.%d.%d %d.%d %s", );
			FILE* fptr = fopen("choice2file.txt", "w");
			fprintf(fptr, "%s", courseData);
			fclose(fptr);
			addToDatabase(databasePtr, "choice2file.txt");
			*/
			choice2(databasePtr);
			break;
		case 3:	;

			printf("Please enter the department number (\"###\"): ");
//			int dept, num;
			scanf("%d", &dept);
			printf("Please enter the course number (\"###\"): ");
			scanf("%d", &num);
			line = findCourse(databasePtr, line, dept, num);
			if (!line) {
				break;
			} else if (line->c1 == 0) {
				fprintf(stderr, "Given course does not exist, EN.%d.%d\n", dept, num);
				break;
			} else {
				printf("%s.%d.%d %d.%d %s\n", line->div, line->dept, line->num, line->c1, line->c2, line->title);          
			}
			break;
		case 4:	;
			displayCourses(databasePtr);
			break;
		case 5:	;

			printf("Please enter the department number (\"###\"): ");
			scanf("%d", &dept);
			printf("Please enter the course number (\"###\"): ");
			scanf("%d", &num);
	//		printf("Please enter the new title (<30 chars): ");
	//		scanf("%s", newTitle);
			changeName(databasePtr, dept, num);
			break;
		case 6:	;
			printf("Please enter the department number (\"###\"): ");
			scanf("%d", &dept);
			printf("Please enter the course number (\"###\"): ");
			scanf("%d", &num);
			
			deleteCourse(databasePtr, dept, num);
			break;
		case 7:	;
			
			printf("Please enter which semester (0-9) you would like to add a course to: ");
			scanf("%d", &sem);
			if (sem < 0 || sem > 9) {
				fprintf(stderr, "Semester out of bounds: %d\n", sem);
				break;
			}
			
			printf("Please enter the department number (\"###\"): ");
			scanf("%d", &dept);
			printf("Please enter the course number (\"###\"): ");
			scanf("%d", &num);
			
			//Course* line = malloc(sizeof(Course));
			line = findCourse(databasePtr, line, dept, num);
			
			if (!line) {
				break;
			} else {
				printf("Please enter your grade for the course (ex: A+, B/, S/): ");
				//grade[0] = getchar();
				//grade[1] = getchar();
				//gets(grade);
				//fgets(grade, 2, stdin);
				
				//scanf("%3s", grade);
				scanf("%s", grade);
				
				if (grade[0] == 'A' || grade[0] == 'B' || grade[0] == 'C' || grade[0] == 'D' || grade[0] == 'F') {
				//if (grade == 'A' || grade == 'B' || grade == 'C' || grade == 'D' || grade == 'F') {
					if (grade[1] != '+' && grade[1] != '-' && grade[1] != '/') {
						fprintf(stderr, "Invalid grade\n");
						break;
					}
				} else if (grade[0] == 'S' || grade[0] == 'U' || grade[0] == 'I') {
					if (grade[1] != '/') {
						fprintf(stderr, "Invalid grade\n");
						break;
					}
				}
				
							
				//semester[sem] = malloc(sizeof(Node));
				//addListingFront(&semester[sem], line, grade);// addFront, need to add
									// to actual
				
				addListing(&semester[sem], line, grade);
				
			}
			
			break;
		case 8:	;
			
			printf("Please enter which semester (0-9) you would like to delete a course from: ");
			scanf("%d", &sem);
			if (sem < 0 || sem > 9) {
				fprintf(stderr, "Semester out of bounds: %d\n", sem);
				break;
			}
			
			printf("Please enter the department number (\"###\"): ");
			scanf("%d", &dept);
			printf("Please enter the course number (\"###\"): ");
			scanf("%d", &num);
			
			//Course* line = malloc(sizeof(Course));
			line = findCourse(databasePtr, line, dept, num);
			
			if (!line) {
				fprintf(stderr, "Course does not exist, %d.%d\n", dept, num);
				break;
			} else {
				deleteListing(&semester[sem], line);
			}
				
			
			break;
		case 9:	;
			printf("Please enter which semester (0-9) you would like to view: ");
			scanf("%d", &sem);
			if (sem < 0 || sem > 9) {
				fprintf(stderr, "Semester out of bounds: %d\n", sem);
				break;
			}
			
			if (!semester[sem]) {
				printf("No courses this semester\n");
			} else {
				printSemList(semester[sem]);
			}
			
			break;
		case 10:;
			
			break;
		case 11:;
	
			break;
		default:
			printf("Restart the program and enter a valid choice.\n");
			input = 0;
		}
		
		dept = 0;
		sem = 0;
		num = 0;
		
	} while (input != 0);
	
	
	
	
	
	
	return 0;
}





