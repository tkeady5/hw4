
#/*
# * Thomas Keady
# * 600.120
# * 3/11/15
# * 516-729-9535
# * tkeady1
# * tkeady1@jhu.edu
# */

# 
# USAGE
#
# make
#  Compiles everything, hw4.c plus tests
#
# make test
#  Performs test
#
# make clean
#  Cleans
#
#


CC=gcc
GPROF= 
CFLAGS = -Wall -Wextra -pedantic -std=c99 -g $(GPROF)
progs = test_list hw4

#bin: hw4 test
bin: hw4 

test: test_list
	@echo "Testing..."
	./test_list
	@echo "Passed all tests."

hw4: hw4.o list.o

hw4.o: hw4.c list.h

list.o: list.c list.h
	$(CC) $(CFLAGS) -c list.c 
# above produces list.o
# .o means no main

test_list: test_list.c list.o
	$(CC) $(CFLAGS) -o test_list test_list.c list.o

#gproftest:
#	make GPROF = "--ftest"
# one way to make the gprof part


clean:
	rm -f *.o $(progs)

