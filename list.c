
/*
 * Thomas Keady
 * 600.120
 * 3/11/15
 * 516-729-9535
 * tkeady1
 * tkeady1@jhu.edu
 */

#include "list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_OF_COURSES 560000	// number of possible courses
#define LENGTH 30 		// max length of course title


/*
 * Creates a database file of 560000 course
 * structs if it doesnt already exist (existence
 * determiend in main where the function is called)
 */
FILE* makeDatabase(FILE* fptr) {
	fseek(fptr, 0, SEEK_SET);		// Cursor to start, define empty struct
	Course empty = { .div = "", .dept = 0, .num = 0, .c1 = 0, .c2 = 0, .title = ""};
	for (int i = 0; i < NUM_OF_COURSES; ++i) {	// Loop 560000 times
		fwrite(&empty, sizeof(Course), 1, fptr);	// and write each time
		//fseek(fptr, sizeof(Course), SEEK_CUR);
	}
	
	return fptr;
	
}


/*
 * Takes a file name (for a file full of course data) and opens it 
 * so that it can put that data into the database
 * Given code is incorporated into loops that go through the given file and 
 * put the information in the desired spot in the database
 * file
 */
void addToDatabase(FILE* databasePtr, char fileName[255]) {
	FILE* fptr = fopen(fileName, "r");			// Open the course file
	
	int cred1, cred2;
	//char div[3] = "", title[LENGTH+1] = "";
	char* divis = (char*)malloc(3);
	char* courseTitle = (char*)malloc(31);
	int deptNum, courseNum;						// Make variables for the info
	int count = 0;
	int c = ' ';
	int lineCount = 0;
	int matches = 0;
					// Given code incorporated here
	while ((matches = fscanf(fptr, "%2s.%d.%d %d.%d ", divis, &deptNum, &courseNum, &cred1, &cred2)) != EOF) {
		++lineCount;			// Keep track of what line we are on
		//printf("inside first while\n");
		if (matches != 5) {
			fprintf(stderr, "File format invalid at line %d\n", lineCount);
								// If the format is incorrect
		} else {
			count = 0;
		//	printf("should be going\n");	
			while (count < LENGTH && (c = fgetc(fptr)) != '\n') {		
				courseTitle[count++] = (char) c;
		//		printf("lo");
			}
			courseTitle[count] = '\0';
		//	printf("how about here?\n");
			while (c != '\n') c = fgetc(fptr);   // skip to end of line
		//	printf("did i get here?\n");
			Course* line = malloc(sizeof(Course));					// Declares the struct for this data
			int location = (deptNum-1)*800 + (courseNum-100);		// Goes from ###.### to 0-559999
			if (location > 559999 || location < 0 || courseNum > 899) {
				fprintf(stderr, "Course number out of bounds, %d.%d\n", deptNum, courseNum);
			} else if (cred1 > 5 || (cred2 != 0 && cred2 != 5)) {		// Error checking
				fprintf(stderr, "Credits out of bounds, %d.%d\n", cred1, cred2);
			} else {
				//Course line {.div = div, .deptNum = dept, .courseNum = num, .c1 = c1, .c2 = c2, .title = title};
				//Course line = {div, dept, num, c1, c2, title};
				//Course line = { {.div = &div}, .deptNum = dept, .courseNum = num, .c1 = c1, .c2 = c2, {.title = &title} };
				//line->div = div;
				//line->div = "";
			//	line->div = ;
				//strcpy(line->div, div);
				strncpy(line->div, divis, 2);
				line->dept = deptNum;
				line->num = courseNum;
				line->c1 = cred1;				// Make the struct
				line->c2 = cred2;
				//line->title = "";
			//	line->title = malloc(31*sizeof(char));
				//strcpy(line->title, title);
				strncpy(line->title, courseTitle, count+1);
				//line->title = title;
				
				//printf("%s.%d.%d %d.%d %s\n", line->div, line->dept, line->num, line->c1, line->c2, line->title);
				printf("%s.%d.%d %d.%d %s\n", divis, line->dept, line->num, line->c1, line->c2, courseTitle);
					
				fseek(databasePtr, location*sizeof(Course), SEEK_SET);	// Move to the right place
				fwrite(line, sizeof(Course), 1, databasePtr);		// Put it there
				//printf("Courses added to database\n");
			}
			
		}	
			
	}
	
	fflush(databasePtr);
	
}


/*
 * Takes the input of course data from stdin, puts it in a file and 
 * sends it to addToDatabase()
 */
void choice2(FILE* databasePtr) {
	
	int c1, c2;
        char div[3], title[LENGTH+1];
        int dept = 0, num = 0;                                          // Make variables for the info
        int count = 0;
        int c = ' ';
        //int lineCount = 0;
        int matches = 0;
	
	
	printf("Please enter the course data (\"__.###.### #.# Title...\"):\n");
	//char courseData[46;				// Prompt and store
	matches = fscanf(stdin, "%2s.%d.%d %d.%d ", div, &dept, &num, &c1, &c2);
	
	
	if (matches != 5) {
		fprintf(stderr, "File format invalid.\n%s %d %d %d %d \n", div, dept, num, c1, c2);
							// If the format is incorrect
		return;
	} else {
		count = 0;
	//	printf("should be going\n");	
		while (count < LENGTH && (c = fgetc(stdin)) != '\n') {		
			title[count++] = (char) c;
	//		printf("lo");
		}
		title[count] = '\0';
	//	printf("how about here?\n");
		while (c != '\n') c = fgetc(stdin);   // skip to end of line
	}
		
	FILE* fptr = fopen("choice2file.txt", "w");				// Opens file,
	fprintf(fptr, "%s.%d.%d %d.%d %s\n", div, dept, num, c1, c2, title);	// puts there,
	fclose(fptr);				
	addToDatabase(databasePtr, "choice2file.txt");			// sends to addToDatabase()
	
	
	
	
}



/*
 * Checks to see if a course 
 * identified by a given number is present in the database
 * Takes the dept and course numbers, goes to the corresponding
 * line in the database (if there is one, error if not) and returns a 
 * pointer to that course
 */
Course* findCourse(FILE* databasePtr, Course* line, int dept, int num) {
	
	int location = (dept-1)*800 + (num-100);		// Goes from ###.### to 1-560000
	if (location > 559999 || location < 0 || num > 899) {	// Numbers out of bounds
		fprintf(stderr, "Given course number out of bounds, EN.%d.%d\n", dept, num);
		return NULL;
	} else {
		fseek(databasePtr, location*sizeof(Course), SEEK_SET);	// Go to the course
		
		fread(line, sizeof(Course), 1, databasePtr);		// Put that info in a struct
	//	if (line.dept == 0 || line.num == 0) {		// If its zero, DNE
	//		fprintf(stderr, "Given course does not exist, EN.%d.%d\n", dept, num);
	//	} else {					// Otherwise...
		return line;					// Return ptr to struct
		printf("%s.%d.%d %d.%d %s\n", line->div, line->dept, line->num, line->c1, line->c2, line->title);
								
	
	}
	
}

/*
 * Prints all the courses in the database
 * For each "line" (Course struct) in the database, 
 * if its course number is not equal to 000.000 (0.0)
 * it is printed with the proper format
 */
void displayCourses(FILE* databasePtr) {
	fseek(databasePtr, 0, SEEK_SET);		// Set cursor to start
	
	int didPrint = 0;
	Course* line = malloc(sizeof(Course));
	
	for (int i = 0; i < 560000; ++i) {		// For this many structs 
		//fseek(databasePtr, i*sizeof(Course), SEEK_SET);	// On to the next one
		//printf("%d\n", i);
		fread(line, sizeof(Course), 1, databasePtr);
		//if (line.dept != 0 && line.num != 0) {	// If its been defined,
		if (line->dept != 0 && line->num != 0) {	// If its been defined,
			//printf("%s.%d.%d %d.%d %s\n", line->div, line->dept, line->num, line->c1, line->c2, line->title);
			didPrint = 1;
			//printf("%d %s.%d.%d %d.%d %s\n", i, line->div, line->dept, line->num, line->c1, line->c2, line->title);
			
			//char* div = (char*)malloc(3);
			//char* title = (char*)malloc(31);
			
			//div = line->div;
			//title = line->title;
			
			//printf("%2s.%d.%d %d.%d %s\n", line.div, line.dept, line.num, line.c1, line.c2, line.title);
			printf("%2s.%d.%d %d.%d %s\n", line->div, line->dept, line->num, line->c1, line->c2, line->title);
			
			//printf("%d.%d %d.%d\n", line->dept, line->num, line->c1, line->c2);
		}					// print
	//	fseek(databasePtr, i*sizeof(Course), SEEK_SET);	// On to the next one

		
	}
	
	if (didPrint == 0) {
		printf("No courses in database\n");
	}
	
	//free(line);
	
}

/*
 * Is fed user ipnut for the course, then prompts for 
 * new title
 * Pulls out the course, changes title and puts it back
 */
void changeName(FILE* databasePtr, int dept, int num) {
	char c = ' ';
	char* newTitle = (char*)malloc(31);;
	int location = (dept-1)*800 + (num-100);		// Goes from ###.### to 0-559999
	if (location > 559999 || location < 0 || num > 899 ) {	// Numbers out of bounds
		fprintf(stderr, "Given course number out of bounds, EN.%d.%d\n", dept, num);
	} else {
		
		printf("Please enter the new title (<30 chars): ");
		scanf("%s", newTitle);
		
		int count = strlen(newTitle);;
	//	printf("should be going\n");	
		while (count < LENGTH && (c = fgetc(stdin)) != '\n') {		
			newTitle[count++] = (char) c;
	//		printf("lo");
		}
		newTitle[count] = '\0';
	//	printf("how about here?\n");
		while (c != '\n') c = fgetc(stdin);   // skip to end of line
		
		printf("location number in changeName: %d\n", location);
		fseek(databasePtr, location*sizeof(Course), SEEK_SET);	// Go to the course
		Course* line = malloc(sizeof(Course));
		fread(line, sizeof(Course), 1, databasePtr);		// Put that info in a struct
		fseek(databasePtr, -1*sizeof(Course), SEEK_CUR);
		
		if (line->dept == 0 || line->num == 0) {		// If its zero, DNE
			fprintf(stderr, "Given course does not exist, EN.%d.%d\n", dept, num);
		} else {					// Otherwise...
			//line->title = "";			// Wipe
			//line->title = newTitle;			// Replace
			strcpy(line->title, "");
			strcpy(line->title, newTitle);
			
			fwrite(line, sizeof(Course), 1, databasePtr);		// Put it back
			printf("Course name changed\n");
		}
	
	}	
	
	
	
}


/*
 * Deletes a course by finding it, then
 * replacing it with an empty struct
 * (declared & initialized within the function)
 */
void deleteCourse(FILE* databasePtr, int dept, int num) {
							// Empty struct VVV
	Course empty = { .div = "", .dept = 0, .num = 0, .c1 = 0, .c2 = 0, .title = ""};
	
	Course* line = malloc(sizeof(Course));		// Holder for the one to delete
	line = findCourse(databasePtr, line, dept, num);// Find it
	if (!line) {					// If its not NULL. 
		return;
	} else if (line->dept == 0 || line->num == 0) {		// or if its zero (DNE)
		fprintf(stderr, "Given course does not exist, EN.%d.%d\n", dept, num);
		return;
	} else {					// Otherwise...
		fseek(databasePtr, ((dept-1)*800 + (num-100))*sizeof(Course), SEEK_SET);
		fwrite(&empty, sizeof(Course), 1, databasePtr);	// Put the cursor there, overwrite
		printf("Course deleted\n");			// old struct with empty
	}
	
	/*
	int location = (dept-1)*800 + (num-100);		// Goes from ###.### to 1-560000
	if (location > 559999 || location < 0 || num > 899) {	// Numbers out of bounds
		fprintf(stderr, "Given course number out of bounds, EN.%d.%d\n", dept, num);
	} else {
		fseek(databasePtr, location*sizeof(Course), SEEK_SET);	// Go to the course
		Course line;
		fread(&line, sizeof(Course), 1, databasePtr);		// Put that info in a struct
		if (line.dept == 0 || line.num == 0) {		// If its zero, DNE
			fprintf(stderr, "Given course does not exist, EN.%d.%d\n", dept, num);
		} else {					// Otherwise...
			fwrite(&empty, sizeof(Course), 1, databasePtr);
			printf("Course deleted\n");
		}
	}
	*/
	
}







int size(const Node* headptr) {
    if (headptr) {
        return 1 + size(headptr->next);
    } else {
        return 0;
    }
}



// Should need a pointer to the node and a pointer to the course ,both malloced beforehand
/*
 * Places a new node at the head of a list
 * Makes the new node, uses Node** to point the next element of the new
 * node to the next node, then switches the pointer that pointed to the
 * next node to now point to itself (effectively inserting itself into the
 * first position of the linked list)
 */
void addListingFront(Node** headptrrf, Course* data, char grade[2]) {
	Node* oldheadptr = *headptrrf;          // Where the list used to start
	Node* newheadptr = malloc(sizeof(Node));// Malloc new node
	newheadptr->next = oldheadptr;          // Point to (now second) node
	*headptrrf = newheadptr;                // Headptr now points to this node (making
						// it the start
	newheadptr->data = data;
	strcpy(newheadptr->grade, grade);		// Transfer over data
	//Node* newheadptr = data;		//-get a new node
	
	/*
	newheadptr->dept = data->dept;                //-populate with data
	newheadptr->num = data->num;                //-populate with data
	newheadptr->c1 = data->c1;       
	newheadptr->c2 = data->c2;       
	newheadptr->gr
	newheadptr->gradeSign = gradeSign; 
	newheadptr->title = title;
	newheadptr->next = oldheadptr;          //-point next to old head
	*headptrrf = newheadptr;                //-update the head by ref
	*/
}



void insert(Node** headptrref, Course* data, char grade[2], int n) {
	Node* temp1 = malloc(sizeof(Node*));
	temp1->data = data;
	strcpy(temp1->grade, grade);
	temp1->next = NULL;
	if (n == 1) {
		temp1->next = *headptrref;
		*headptrref = temp1;
		return;
	}
	Node* temp2 = *headptrref;
	for (int i = 0; i < n-2; ++i) {
		temp2 = temp2->next;
	}
	temp1->next = temp2->next;
	temp2->next = temp1;
	
	
}



 
int walk(Node* headptr, int deptNum, int courseNum, int counter) {
	
	if (headptr == NULL) {
		return counter;
	} else {
		
		int location = (headptr->data->dept -1)*800 + headptr->data->num -100;
		int wantLocation = (deptNum -1)*800 + courseNum -100;
		
		if (location > wantLocation) {
			return counter;
		} else if (location < wantLocation) {
			return counter+1;
		} else {
			return walk(headptr->next, deptNum, courseNum, counter+1);
		}
	
	}
	
}






/*
 * Walks through linked list, checking the course dept and numbers with 
 * the numbers being searched for 
 * The first time the course to be inserted has greater numbers than the course in 
 * the list, it gets inserted there by calling addListingFront() with the pointer to 
 * that pointer as the headptr of a sublist, such that the list always remains
 * in order
 */
void addListing(Node** headptrref, Course* courseData, char grade[2]) {
	int placement = walk(*headptrref, courseData->dept, courseData->num, 1);	
	insert(headptrref, courseData, grade, placement);
	
	/*aint listSize;
	if ((listSize = size(*headptrref)) == 0) {
		addListingFront(headptrref, courseData, grade);
	} else {
		insert(headptrref, courseData, grade, listSize);
	}
	
	for (; *headptrref; headptrref = &((*headptrref)->next)) {	// Walk through the list
		
	//	printf("%d\n", (*headptrref)->data->dept);
		printf("%d\n", courseData->dept);
		printf("%d\n", (*headptrref)->*data->num);
		printf("%d\n", courseData->num); 
		
		(*headptrref)->data = courseData;
		
		//
		//Course* toAdd = (*headptrref)->data;
		//Course* toAdd = &((*headptrref)->data);
		
		if ((*headptrref)->data->dept <= courseData->dept && ((*headptrref)->data->num <= courseData->num)) {
			addListingFront(headptrref, courseData, grade);// If this is where it 
			return;						    // should go, call it
		}
	} */
	
}


/*
 * Deletes the first node in a linked list
 * If there is one (the list is not empty), remove it
 * by freeing it and making the pointer point to the next one
 */
void deleteListingFront(Node** headptrref) {
	Node* oldheadptr = *headptrref;		// The node to delete
	if (oldheadptr) {
		free(oldheadptr->data);		// may not be necessary
		*headptrref = oldheadptr->next; // Point past it
		free(oldheadptr);		// before you free it
		//free(oldheadptr->next);
		//free(oldheadptr->data);
	
	} else {						// If there is no list,
		fprintf(stderr, "Course not in listing\n");	// notify
	}
	
}


//void choice7checking




void deleteListing(Node** headptrref, Course* data) {
	for (; *headptrref; headptrref = &((*headptrref)->next)) {
		printf("%d\n", (*headptrref)->data->dept);
		printf("%d\n", data->dept);
		printf("%d\n", (*headptrref)->data->num);
		printf("%d\n", data->num);
		
		if ((*headptrref)->data->dept == data->dept && ((*headptrref)->data->num == data->num)) {
			deleteListingFront(headptrref);
			
		}
	
	}
	
}








void printSemList(Node* headptr) {
    for (; headptr; headptr = headptr->next) {
        printf("EN.%d.%d %d.%d %s\n", (headptr->data)->dept, headptr->data->num, headptr->data->c1, headptr->data->c2, headptr->data->title);
    }
}





/*

int size(const Node* headptr) {
    if (headptr) {
        return 1 + size(headptr->next);
    } else {
        return 0;
    }
}


void fprintList(FILE* outfile, const Node* headptr) {
    for (; headptr; headptr = headptr->next) {
        fprintf(outfile, "EN.%d.%d %d.%d %s\n", (headptr->data)->deptNum, headptr->data->courseNum, headptr->data->c1, headptr->data->c2, headptr->data->title);
    }
}


void fprintListR(FILE* outfile, const Node* headptr) {
    if (headptr) {
        fprintf(outfile, "EN.%d.%d %d.%d %s\n", (headptr->data)->deptNum, headptr->data->courseNum, headptr->data->c1, headptr->data->c2, headptr->data->title);
        fprintListR(outfile, headptr->next);
    }
}

void fprintListRev(FILE* outfile, const Node* headptr) {
    if (headptr) {
        fprintListRev(outfile, headptr->next);
        fprintf(outfile, "EN.%d.%d %d.%d %s\n", (headptr->data)->deptNum, headptr->data->courseNum, headptr->data->c1, headptr->data->c2, headptr->data->title);
    }
}


void printList(const Node* headptr) {
    fprintList(stdout, headptr);
}


void printListR(const Node* headptr) {
    fprintListR(stdout, headptr);
}

void printListRev(const Node* headptr) {
    fprintListRev(stdout, headptr);
}

//cross this bridge when i get here
void addFront(Node** headptrrf, Course data) {
    Node* oldheadptr = *headptrrf;          //-to keep the *s straight
    Node* newheadptr = malloc(sizeof(Course));//-get a new node
    newheadptr->dept = dept;                //-populate with data
    newheadptr->num = num;                //-populate with data
    newheadptr->c1 = c1;       
    newheadptr->c2 = c2;       
    newheadptr->grade = grade;
    newheadptr->gradeSign = gradeSign;
    newheadptr->title = title;
    newheadptr->next = oldheadptr;          //-point next to old head
    *headptrrf = newheadptr;                //-update the head by ref
}

int deleteFront(Node** headptrref) {
    Node* oldheadptr = *headptrref;
    if (oldheadptr) {
        *headptrref = oldheadptr->next;
        free(oldheadptr);
        //free(oldheadptr->next);
	//free(oldheadptr->data);

	return 1;
    } else {
        return 0;
    }
}

int delete(Node** headptrref, int todeleteD, int todeleteC) {
    for (; *headptrref; headptrref = &((*headptrref)->next)) {
        if ((*headptrref)->deptNum == todeleteD && ((*headptrref)->courseNum == todeleteC)) {
            return deleteFront(headptrref);
        }
    }
    return 0;
}

int deleteR(Node** headptrref, int todeleteD, int todeleteC) {
    Node* oldheadptr = *headptrref;
    if (!oldheadptr) {                  // if list is empty, give up
        return 0;                       
    } else if (oldheadptr->deptNum == todeleteD || oldheadptr->courseNum == todeleteC) {
        return deleteFront(headptrref); // if frst node matches, rm it
    } else {                            // otherwise, recurse
        return deleteR(&oldheadptr->next, todeleteD, todeleteC); 
    }
}

void clearList(Course** headptrref) {
    Course* oldheadptr = *headptrref;
    if (oldheadptr) {                // if the list isn't empty
        clearList(&oldheadptr->next);// clear the rest of the list
        free(oldheadptr);            // free the remaining first node
        *headptrref = NULL;          // make the headptr point to NULL
    }
}

// // Alternative version of clearList that uses deleteFront
//void clearList(Course** headptrref) {
//    while (*headptrref) {
//        deleteFront(headptrref);
//    }
//}

Course* find(Course* headptr, int tofindD, int tofindC) {
    if (headptr == NULL || (headptr->deptNum == tofindD && headptr->courseNum == tofindC)) {
        return headptr;
    } else {
        return find(headptr->next, tofindD, tofindC);
    }
}

int replace(Course* headptr, int oldD, int oldC, int newD, int newC, float creds, char* title) {
    Course* toChange = find(headptr, oldD, oldC);
    if (toChange) {
        toChange->deptNum = newD;
        toChange->courseNum = newC;
	toChange->creds = creds;
	toChange->title = title;
        return 1;
    } else {
        return 0;
    }
} */
