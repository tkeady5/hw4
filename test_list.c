#include "list.h"
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match.
 */
bool fileeq(char lhsName[], char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    // don't compare if we can't open the files
    if (!lhs || !rhs) return false;

    bool match = true;
    // read until both of the files are done or there is a mismatch
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = false;
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}

/*
 * Test fileeq on same file, files with same contents, files with
 * different contents and a file that doesn't exist
 */
void test_fileeq() {
    FILE* fptr = fopen("test1.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    fptr = fopen("test2.txt", "w");
    fprintf(fptr, "this\nis\na different test\n");
    fclose(fptr);

    fptr = fopen("test3.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    assert(fileeq("test1.txt", "test1.txt"));
    assert(fileeq("test2.txt", "test2.txt"));
    assert(!fileeq("test2.txt", "test1.txt"));
    assert(!fileeq("test1.txt", "test2.txt"));
    assert(fileeq("test3.txt", "test3.txt"));
    assert(fileeq("test1.txt", "test3.txt"));
    assert(fileeq("test3.txt", "test1.txt"));
    assert(!fileeq("test2.txt", "test3.txt"));
    assert(!fileeq("test3.txt", "test2.txt"));
    assert(!fileeq("", ""));  // can't open file
}



void test_makeDatabase() {
	FILE* dexpected = fopen("dexpected.dat");
	
	
	
	
	
}




int main (void) {
	printf("Testing makeDatabase...\n");
	test_makeDatabase();
	
	
	return 0;
}


/*
###
 * Tests the size() function which returns the 
 * length of a linked list
 * Creates a list with 4 nodes, checks the length of
 * the list and each sub list
 ^^^
void testsize() {
    Course c1 = {.deptNum = 3};
    Course c2 = {.deptNum = 2};
    Course c3 = {.deptNum = 6};
    Course c4 = {.deptNum = 16};
    c1.next = &c2;
    c2.next = &c3;
    c3.next = &c4;
    c4.next = NULL;
    assert(size(&c1) == 4);
    assert(size(&c2) == 3);
    assert(size(&c3) == 2);
    assert(size(&c4) == 1);
    assert(size(NULL) == 0);
}


###
 * Checks to make sure that fprintList is working
 * Opens a file (fexpected.txt) and prints the expected result to it
 * Opens a file (fobserved.txt) and prints the results of fprintList()
 * to it
 * Asserts that the files are equivalent
 * This function must be given the expected file contents and the
 * start pointer (headptr) to the list
 ^^^
void checkList(Course* headptr, char* expectedPrint) {
    FILE* fexpected = fopen("fexpected.txt", "w");
    assert(fexpected);
    fprintf(fexpected, expectedPrint);
    fclose(fexpected);
    
    FILE* fobserved = fopen("fobserved.txt", "w");
    assert(fobserved);
    fprintList(fobserved, headptr);
    fclose(fobserved);
    
    assert(fileeq("fexpected.txt", "fobserved.txt"));
}

###
 * Performs the same function as checkList() but for the recursive 
 * version of fprintList() (fprintListR())
 ^^^
void checkListR(Course* headptr, char* expectedPrint) {
    FILE* fexpected = fopen("fexpected.txt", "w");
    assert(fexpected);
    fprintf(fexpected, expectedPrint);
    fclose(fexpected);
    
    FILE* fobserved = fopen("fobserved.txt", "w");
    assert(fobserved);
    fprintListR(fobserved, headptr);
    fclose(fobserved);
    
    assert(fileeq("fexpected.txt", "fobserved.txt"));
}

###
 * Performs the same function as checkList() but for the reversed recursive 
 * version of fprintList() (fprintListRev())
 ^^^
void checkListRev(Course* headptr, char* expectedPrint) {
    FILE* fexpected = fopen("fexpected.txt", "w");
    assert(fexpected);
    fprintf(fexpected, expectedPrint);
    fclose(fexpected);
    
    FILE* fobserved = fopen("fobserved.txt", "w");
    assert(fobserved);
    fprintListRev(fobserved, headptr);
    fclose(fobserved);
    
    assert(fileeq("fexpected.txt", "fobserved.txt"));
}

###
 * Generates two linked lists and tests all 3 printing functions on 
 * both of them
 * We assume that the print functions to stdout do not need to be tested
 * since they simply call the functions tested here with stdout
 * as the "file" they print to
 ^^^
void testfprintList() {
	Course c1 = {.deptNum = 600, .courseNum = 120, .creds = 4.0, .title = "Intermediate Programming"};
	Course c2 = {.deptNum = 600, .courseNum = 121, .creds = 4.0, .title = "another hard class"};
	Course c3 = {.deptNum = 600, .courseNum = 320, .creds = 4.5, .title = "Advanced programming"};
	c1.next = &c2;
	c2.next = &c3;
	c3.next = NULL;
	checkList(&c1, "EN.600.120 4.0 Intermediate Programming\nEN.600.121 4.0 another hard class\nEN.600.320 4.5 Advanced programming\n");
	checkListR(&c1, "EN.600.120 4.0 Intermediate Programming\nEN.600.121 4.0 another hard class\nEN.600.320 4.5 Advanced programming\n");
	checkListRev(&c1, "EN.600.320 4.5 Advanced programming\nEN.600.121 4.0 another hard class\nEN.600.120 4.0 Intermediate Programming\n");
	
	###
	Course n4 = {.data = 123};
	Course n5 = {.data = 12};
	Course n6 = {.data = 123456};
	n4.next = &n5;
	n5.next = &n6;
	n6.next = NULL;
	checkList(&n4, "123 12 123456 ");
	checkListR(&n4, "123 12 123456 ");
	checkListRev(&n4, "123456 12 123 ");
	^^^
}


###
 * Tests that addFront() works by giving it a node 
 * to add to the front of the list, and then checking
 * that the list now checks out with the expected result
 * by calling checkList(), which prints the lists to files and
 * asserts that they are printing properly
 * Since all of the printing functions have been tested, this one only
 * uses the first one to check the addFront() function
 ^^^
void testaddFront() {
    Course* headptr = NULL;
    addFront(&headptr, 600, 120, 4.0, 'A', '+', "Intermediate Programming");
    checkList(headptr, "EN.600.120 4.0 Intermediate Programming\n");
    addFront(&headptr, 600, 121, 4.0, 'C', '+', "another hard course");
    checkList(headptr, "EN.600.121 4.0 another hard course\nEN.600.120 4.0 Intermediate Programming\n");
    addFront(&headptr, 600, 320, 4.5, 'F', '-', "Advanced programming");
    checkList(headptr, "EN.600.320 4.5 Advanced programming\nEN.600.121 4.0 another hard course\nEN.600.120 4.0 Intermediate Programming\n");


}

###
 * Works the same way as testaddFront(), but makes a linked list first
 * so that can delete elements as it goes
 * Also checks the result for an empty list
 * Will factor the node-generating section of code and the freeing secion
 * of code into functions later 
 ^^^
void testdeleteFront() {
	###
	Course* c6 = malloc(sizeof(Course));
	n6->next = NULL;
	Course* c5 = malloc(sizeof(Course));
	c5->next = c6;
	Course* c4 = malloc(sizeof(Course));
	c4->next = c5;
	^^^
	Course* c3 = malloc(sizeof(Course));	// Make linked list
	c3->next = NULL;
	Course* c2 = malloc(sizeof(Course));
	c2->next = c3;
	Course* c1 = malloc(sizeof(Course));
	c1->next = c2;
	
	Course** headptr = &c1;			// Points to the head of the list
	
	c3->deptNum = 3;
	c3->courseNum = 3;
	c3->creds = 3.0;
	c3->title = "Three";
	
	c2->deptNum = 2;
	c2->courseNum = 2;
	c2->creds = 2.0;
	c2->title = "Two";
	
	c1->deptNum = 1;
	c1->courseNum = 1;
	c1->creds = 1.0;
	c1->title = "One";
	
						// Start checking
	
	checkList(*headptr, "EN.1.1 1.0 One\nEN.2.2 2.0 Two\nEN.3.3 3.0 Three\n");
	deleteFront(headptr);
	checkList(*headptr, "EN.2.2 2.0 Two\nEN.3.3 3.0 Three\n");
	deleteFront(headptr);
	checkList(*headptr, "EN.3.3 3.0 Three\n");
	deleteFront(headptr);
	checkList(*headptr, "");		// Works on empty lists...
	
	assert(deleteFront(headptr) == 0);	// Check
	

}

###
 * Tests the delete function, which deletes any node in the 
 * list (specified by its data value) by calling deleteFront() with
 * the node to be deleted at the front of s sub-list
 * Like testdeleteFront, makes a linked list and checks with checkList, but this time
 * deletes them our of order
 ^^^
void testdelete() {
	###
	Course* n6 = malloc(sizeof(Course));
	n6->data = 6;
	n6->next = NULL;
	Course* n5 = malloc(sizeof(Course));
	n5->data = 5;
	n5->next = n6;
	Course* n4 = malloc(sizeof(Course));
	n4->data = 4;
	n4->next = n5;
	Course* n3 = malloc(sizeof(Course));	// Make linked list
	n3->data = 3;
	n3->next = n4;
	Course* n2 = malloc(sizeof(Course));
	n2->data = 2;
	n2->next = n3;
	Course* n1 = malloc(sizeof(Course));
	n1->data = 1;
	n1->next = n2;
	
	Course** headptr = &n1;			// Points to the head of the list
	
	checkList(*headptr, "1 2 3 4 5 6 ");
	delete(headptr, 6);
	checkList(*headptr, "1 2 3 4 5 ");
	delete(headptr, 2);
	checkList(*headptr, "1 3 4 5 ");	// Chops off the first node,
	delete(headptr, 3);			// Makes sure list is as expected
	checkList(*headptr, "1 4 5 ");
	delete(headptr, 1);
	checkList(*headptr, "4 5 ");
	delete(headptr, 4);
	checkList(*headptr, "5 ");
	delete(headptr, 5);
	checkList(*headptr, "");
	^^^
	
	Course* c3 = malloc(sizeof(Course));	// Make linked list
	c3->next = NULL;
	Course* c2 = malloc(sizeof(Course));
	c2->next = c3;
	Course* c1 = malloc(sizeof(Course));
	c1->next = c2;
	
	Course** headptr = &c1;			// Points to the head of the list
	
	c3->deptNum = 3;
	c3->courseNum = 3;
	c3->creds = 3.0;
	c3->title = "Three";
	
	c2->deptNum = 2;
	c2->courseNum = 2;
	c2->creds = 2.0;
	c2->title = "Two";
	
	c1->deptNum = 1;
	c1->courseNum = 1;
	c1->creds = 1.0;
	c1->title = "One";
	
						// Start checking

	checkList(*headptr, "EN.1.1 1.0 One\nEN.2.2 2.0 Two\nEN.3.3 3.0 Three\n");
	delete(headptr, 3, 3);
	checkList(*headptr, "EN.1.1 1.0 One\nEN.2.2 2.0 Two\n");
	delete(headptr, 1, 1);
	checkList(*headptr, "EN.2.2 2.0 Two\n");
	delete(headptr, 2, 2);
	checkList(*headptr, "");		// Works on empty lists...
	
	
	
		
}


###
 * Tests the recursive deletion function, exactly the same as above
 * testdelete() but calls deleteR() 
 ^^^
void testdeleteR() {
	###
	Course* n6 = malloc(sizeof(Course));
	n6->data = 6;
	n6->next = NULL;
	Course* n5 = malloc(sizeof(Course));
	n5->data = 5;
	n5->next = n6;
	Course* n4 = malloc(sizeof(Course));
	n4->data = 4;
	n4->next = n5;
	Course* n3 = malloc(sizeof(Course));	// Make linked list
	n3->data = 3;
	n3->next = n4;
	Course* n2 = malloc(sizeof(Course));
	n2->data = 2;
	n2->next = n3;
	Course* n1 = malloc(sizeof(Course));
	n1->data = 1;
	n1->next = n2;
	
	Course** headptr = &n1;			// Points to the head of the list
	
	checkList(*headptr, "1 2 3 4 5 6 ");
	deleteR(headptr, 6);
	checkList(*headptr, "1 2 3 4 5 ");
	deleteR(headptr, 2);
	checkList(*headptr, "1 3 4 5 ");
	deleteR(headptr, 3);			// Starts deleting random 
	checkList(*headptr, "1 4 5 ");		// nodes, checks the remaining
	deleteR(headptr, 1);			// list for correctness
	checkList(*headptr, "4 5 ");
	deleteR(headptr, 4);
	checkList(*headptr, "5 ");
	deleteR(headptr, 5);
	checkList(*headptr, "");
	^^^
	
	Course* c3 = malloc(sizeof(Course));	// Make linked list
	c3->next = NULL;
	Course* c2 = malloc(sizeof(Course));
	c2->next = c3;
	Course* c1 = malloc(sizeof(Course));
	c1->next = c2;
	
	Course** headptr = &c1;			// Points to the head of the list
	
	c3->deptNum = 3;
	c3->courseNum = 3;
	c3->creds = 3.0;
	c3->title = "Three";
	
	c2->deptNum = 2;
	c2->courseNum = 2;
	c2->creds = 2.0;
	c2->title = "Two";
	
	c1->deptNum = 1;
	c1->courseNum = 1;
	c1->creds = 1.0;
	c1->title = "One";
	
						// Start checking

	checkList(*headptr, "EN.1.1 1.0 One\nEN.2.2 2.0 Two\nEN.3.3 3.0 Three\n");
	deleteR(headptr, 3, 3);
	checkList(*headptr, "EN.1.1 1.0 One\nEN.2.2 2.0 Two\n");
	deleteR(headptr, 1, 1);
	checkList(*headptr, "EN.2.2 2.0 Two\n");
	deleteR(headptr, 2, 2);
	checkList(*headptr, "");		// Works on empty lists...
	
	
	
		
}

###
 * Makes a list (not unneccessarily large) and 
 * makes sure that clearList clears it correctly
 ^^^
void testclearList() {
	###
	Course* n2 = malloc(sizeof(Course));
	n2->data = 2;
	n2->next = NULL;				// Make (short) linked list
	Course* n1 = malloc(sizeof(Course));
	n1->data = 1;
	n1->next = n2;
	
	Course** headptr = &n1;			// Points to the head of the list
	
	checkList(*headptr, "1 2 ");		// Make sure this is good
	clearList(headptr);			// Clear the list
	checkList(*headptr, "");		// Make sure its empty now
	clearList(headptr);			// Make sure no errors if
	^^^					// given empty list
	
	Course* c2 = malloc(sizeof(Course));
	c2->next = NULL;
	Course* c1 = malloc(sizeof(Course));
	c1->next = c2;
	
	Course** headptr = &c1;			// Points to the head of the list
	
	c2->deptNum = 2;
	c2->courseNum = 2;
	c2->creds = 2.0;
	c2->title = "Two";
	
	c1->deptNum = 1;
	c1->courseNum = 1;
	c1->creds = 1.0;
	c1->title = "One";
	
	checkList(*headptr, "EN.1.1 1.0 One\nEN.2.2 2.0 Two\n");
	clearList(headptr);
	checkList(*headptr, "");
	
	
}

###
 * Test the find() function, which searches for
 * a node with the given data value
 * This function makes sure it can find the node with
 * the data value (as long as it is the right type) and
 * behaves properly when no such value is found (returns NULL)
 ^^^
void testfind() {
	###
	Course* n6 = malloc(sizeof(Course));
	n6->data = 6;
	n6->next = NULL;
	Course* n5 = malloc(sizeof(Course));
	n5->data = 5;
	n5->next = n6;
	Course* n4 = malloc(sizeof(Course));
	n4->data = 4;
	n4->next = n5;
	Course* n3 = malloc(sizeof(Course));	// Make linked list
	n3->data = 3;
	n3->next = n4;
	Course* n2 = malloc(sizeof(Course));
	n2->data = 2;
	n2->next = n3;
	Course* n1 = malloc(sizeof(Course));
	n1->data = 1;
	n1->next = n2;
	
	Course** headptr = &n1;			// Points to the head of the list
	
	assert(find(*headptr, 6) == n6);	//
	assert(find(*headptr, 1) == n1);	// These two check ends
	assert(find(*headptr, 3) == n3);	// Check middle
	assert(find(*headptr, 7) == NULL);	// Checks if not found
	//assert(find(*headptr, "um") == NULL);	// Assume only search for proper data type??
	^^^
	
	Course* c3 = malloc(sizeof(Course));	// Make linked list
	c3->next = NULL;
	Course* c2 = malloc(sizeof(Course));
	c2->next = c3;
	Course* c1 = malloc(sizeof(Course));
	c1->next = c2;
	
	Course** headptr = &c1;			// Points to the head of the list
	
	c3->deptNum = 3;
	c3->courseNum = 3;
	c3->creds = 3.0;
	c3->title = "Three";
	
	c2->deptNum = 2;
	c2->courseNum = 2;
	c2->creds = 2.0;
	c2->title = "Two";
	
	c1->deptNum = 1;
	c1->courseNum = 1;
	c1->creds = 1.0;
	c1->title = "One";
	
	assert(find(*headptr, 3, 3) == c3);
	assert(find(*headptr, 2, 2) == c2);
	assert(find(*headptr, 1, 1) == c1);
	assert(find(*headptr, 0, 0) == NULL);
	
	
}

###
 * Makes sure that the replace() function works
 * by creating a linked list, replacing some nodes
 * and making sure that it all checks out
 * Also makes sure that replace() properly returns NULL
 * when the node it is supposed to replace does not exist
 ^^^
void testreplace() {
	###
	Course* n6 = malloc(sizeof(Course));
	n6->data = 6;
	n6->next = NULL;
	Course* n5 = malloc(sizeof(Course));
	n5->data = 5;
	n5->next = n6;
	Course* n4 = malloc(sizeof(Course));
	n4->data = 4;
	n4->next = n5;
	Course* n3 = malloc(sizeof(Course));	// Make linked list
	n3->data = 3;
	n3->next = n4;
	Course* n2 = malloc(sizeof(Course));
	n2->data = 2;
	n2->next = n3;
	Course* n1 = malloc(sizeof(Course));
	n1->data = 1;
	n1->next = n2;
	
	Course** headptr = &n1;			// Points to the head of the list
	
	checkList(*headptr, "1 2 3 4 5 6 ");
	replace(*headptr, 1, 10);
	checkList(*headptr, "10 2 3 4 5 6 ");
	replace(*headptr, 10, 11);
	checkList(*headptr, "11 2 3 4 5 6 ");	// Make sure it replaces properly
	replace(*headptr, 4, 42);
	checkList(*headptr, "11 2 3 42 5 6 ");
	replace(*headptr, 100, 1000);
	checkList(*headptr, "11 2 3 42 5 6 ");
	
	assert(replace(*headptr, 100, 1000) == 0);// And returns properly
	^^^
	
	Course* c3 = malloc(sizeof(Course));	// Make linked list
	c3->next = NULL;
	Course* c2 = malloc(sizeof(Course));
	c2->next = c3;
	Course* c1 = malloc(sizeof(Course));
	c1->next = c2;
	
	Course** headptr = &c1;			// Points to the head of the list
	
	c3->deptNum = 3;
	c3->courseNum = 3;
	c3->creds = 3.0;
	c3->title = "Three";
	
	c2->deptNum = 2;
	c2->courseNum = 2;
	c2->creds = 2.0;
	c2->title = "Two";
	
	c1->deptNum = 1;
	c1->courseNum = 1;
	c1->creds = 1.0;
	c1->title = "One";
	
	checkList(*headptr, "EN.1.1 1.0 One\nEN.2.2 2.0 Two\nEN.3.3 3.0 Three\n");
	replace(*headptr, 3, 3, 4, 4, 4.0, "Four");
	checkList(*headptr, "EN.1.1 1.0 One\nEN.2.2 2.0 Two\nEN.4.4 4.0 Four\n");
	replace(*headptr, 9, 9, 4, 5, 4.5, "Forty-five");
	checkList(*headptr, "EN.1.1 1.0 One\nEN.2.2 2.0 Two\nEN.4.4 4.0 Four\n");
	
	assert(replace(*headptr, 9, 9, 4, 5, 4.5, "Forty-five") == 0);
}


###
 * Calls all of our test functions
 ^^^
int main(void) {
	testsize();
	printf("Passed size tests...\n");
	testfprintList();
	printf("Passed printList tests...\n");
	testaddFront();
	printf("Passed addFront tests...\n");
	testdeleteFront();
	printf("Passed deleteFront tests...\n");
	testdelete();
	printf("Passed delete tests...\n");
	testdeleteR();
	printf("Passed deleteR tests...\n");
	testclearList();
	printf("Passed clearList tests...\n");
	testfind();
	printf("Passed find tests...\n");
	testreplace();
	printf("Passed replace tests...\n");
	
	
	return 0;
}


*/
