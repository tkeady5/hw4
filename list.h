
/*
 * Thomas Keady
 * 600.120
 * 3/11/15
 * 516-729-9535
 * tkeady1
 * tkeady1@jhu.edu
 */

#ifndef LIST_H
#define LIST_H

#include <stdio.h>

struct course {
	//char* div = malloc(3*sizeof(char));
	char div[3];
	int dept;
	int num;
	int c1;
	int c2;		// c1.c2 = credits (c2 can only be 0 or 5);
	//char* title = malloc(31*sizeof(char));
	char title[31];
	//char grade;
	//char gradeSign;
//	struct course* next;
};

typedef struct course Course;


struct node {
	
	//struct Course* courseData; 		// Will we do it like this, or put the course 
	struct node * next;			// data into the struct node?
	Course* data;	// make pointer (if want to change data which i do)
	char grade[2];
	//char gradeSign;
};

typedef struct node Node;

			// GET TO WORK ON REDOING THE FUNCTIONS


FILE* makeDatabase(FILE* fptr);




void addToDatabase(FILE* databasePtr, char fileName[255]);



void choice2(FILE* databasePtr);



Course* findCourse(FILE* databasePtr, Course* line, int dept, int num);



void displayCourses(FILE* databasePtr);



void changeName(FILE* databasePtr, int dept, int num);



void deleteCourse(FILE* databasePtr, int dept, int num);


int size(const Node* headptr);



void addListingFront(Node** headptrref, Course* data, char* grade);


void insert(Node** headptrref, Course* data, char grade[2], int n);


int walk(Node* headptr, int deptNum, int courseNum, int counter);


void addListing(Node** headptrref, Course* data, char* grade);


void deleteListingFront(Node** headptrref);


void deleteListing(Node** headptrref, Course* data);


void printSemList(Node* headptr);



/*
 * Returns the size of the linked-list headed by *headptr
 * (size(NULL) is 0)
 ###
int size(const Node* headptr);


 * Each item in the list headed by *headptr is printed to outfile
 * followed by a single space (items are printed in the order found in
 * the list)
 ###
void fprintList(FILE* outfile, const Node* headptr);

*
 * Alternative recursive implementation of (fprintList)
 ###
void fprintListR(FILE* outfile, const Node* headptr);

*
 * In reverse order, each item in the list headed by *headptr is
 * printed to outfile followed by a single space
 ###
void fprintListRev(FILE* outfile, const Node* headptr);

*
 * specialized versions of the above functions that print to stdout
 ###
void printList(const Node* headptr);
void printListR(const Node* headptr);
void printListRev(const Node* headptr);

*
 * the list referenced by headptrrf (which points to a pointer to the
 * first node of some linked list or points to NULL for an empty list)
 * is modified to include an additional node with the specificed data
 * followed by the elements of the original list
 ###
//void addFront(Course** headptrrf, int deptNum, int courseNum, float creds, char grade, char gradeSign, char* title);
void addFront(Node** headptrrf, Course data);


*
 * the list referenced by headptrrf (which points to a pointer to the
 * first node of some linked list or points to NULL for an empty list)
 * is modified to remove the first node if there is one; if there was
 * a node to remove, the function returns 1, if there was no node to
 * remove, the function returns 0
 ###
int deleteFront(Node** headptrref);

*
 * the list referenced by headptrrf (which points to a pointer to the
 * first node of some linked list or points to NULL for an empty list)
 * is modified to remove the first node with data equal to 'todelete';
 * 1 is returned if such a node was found and 0 otherwise
 ###
//int delete(Course** headptrref, int todelete);
int delete(Node** headptrref, int todeleteD, int todeleteC);

*
 * Alternative recursive implementation of (delete)
 ###
int deleteR(Node** headptrref, int todeleteD, int todeleteC);
*
 * the list referenced by headptrrf (which points to a pointer to the
 * first node of some linked list or points to NULL for an empty list)
 * is modified to remove all nodes from the list
 ###
void clearList(Node** headptrref);

*
 * Returns a pointe to the first node with data matching tofind or
 * NULL if no such node is found
 ###
Node* find(Node* headptr, int tofindD, int tofindC);

*
 * Finds the first node with data matching old and replaces it with
 * new; returns 1 if the replace was succesful and 0 if no matching
 * node was found
 ###
//int replace(Course* headptr, int oldD, int oldC, int newD, int newC, float creds, char* title);
int replace(Node* headptr, int oldD, int oldC, int newD, int newC, float creds, char* title);
*/
#endif
